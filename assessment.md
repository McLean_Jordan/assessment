#Development Experience#
Recently I have been using TeraData SQL and some SAS to research and capture customer and account data for my role in doing customer remediation work at Wells Fargo. In my previous roles, I have used C#, VB .Net, SQL Server, and before that Visual Basic 6 and SAS.
I like both VB .Net and C#, but if I had to choose, I would prefer C# as i think the syntax is more concise. To me, the If/End If and Case/End Case etc. of VB .Net are too wordy and i like the {} syntax of C#. Coming from a VB6 environment, VB .Net was familiar, but once a developer works in C# i believe it is preferable.
#Advice#
I would have two pieces of advice that I think are the best I have gotten. The first would be don't be afraid to ask questions. I feel that I am humble enough to know that I do not know everything and there will always be someone that knows more about something than I do. We can lean on each other's knowledge to develop better products and no question is off limits.
The second piece iof advice that I have gotten is that money is not everything. Personally as a husband and father, I have come to value this advice and the time I can spend with my family. I like to work and build good products for my customers, but I also value a good work/life balance that allows me to be a developer and have personal time with family also.

The sum of the even numbers in the Fibonacci sequence that are less than 4MM is 4613732.  

Below is code for a C# console application that i created to calculate the answer.
using System;
using System.Collections;
using System.Collections.Generic;
 
 
namespace ITATest
{
                class clsFibonacci
                {
                                static void Main()
                                {
                                                long sum = 0;
                                                List<long> fibList = new List<long>();
                                                fibList = fibSeries();
 
                                                foreach (long item in fibList)
                                                {                                                             
                                                                //Console.WriteLine(item);
                                                                sum+=item;
                                                }                             
 
                                                Console.WriteLine(sum);
                                               
                                                Console.ReadLine();
                                }             
                               
                                static List<long> fibSeries()
                                {
                                                long a = 0;
                                                long b = 1;
                                                long next = a+b;
 
                                                List<long> fibList = new List<long>();
                                               
                                                while (next < 4000000)
                                                {                                                                                                                             
                                                                if (next % 2 == 0)
                                                                {
                                                                                fibList.Add(next);
                                                                }                                                                                                             
 
                                                                a = b;
                                               b = next;
                                                                next = a + b;
                                                }
                                               
                                                return fibList;
                                                                                               
                                }             
                }
}



Below is the code for a test.html page that i created containing javascript with an artray and a loop to calculate.

<!DOCTYPE html>
<html>
<body onload="sumFibonacci()">
 
<h1>ITA Test</h1>
 
<script>
function sumFibonacci()
{   
                var sum = 0;   
                // code logic for adding all the numbers of the array  
 
                var fibonacciArray = fibSeries();   
               
                for(var i=0, n=fibonacciArray.length; i<n; i++)
                {             
                                alert(fibonacciArray[i]);
                                sum += fibonacciArray[i];
                }
                alert(sum);
                //return sum;
}
 
function fibSeries() {   
                var a = 0;
                var b = 1;   
                var next = a + b;
 
                var fibonacciArray = [];   
 
                while(next < 4000000)
                {
                                if(next % 2 === 0)
                                {
                               fibonacciArray.push(next);
               }
                               
                                a = b;
               b = next;
                                next = a + b;
                }   
 
                return fibonacciArray;
 
}
</script>
 
</body>
</html>